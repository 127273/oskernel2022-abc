i = 0;
hexArr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']
with open ("../xv6-user/initcode", "rb") as f:
    f1 = open("code.txt", "wb");
    for word in f.read():
        #print(str(hex(word)) + ",")
        if(int(word) < 0x10):
        
            word = str("0x0") + hexArr[int(word)]
            f1.write(((word).encode()))
        else:
            f1.write(bytes(hex(word).encode()))
        f1.write(", ".encode())
        i += 1
        if(i % 16 == 0):
            f1.write("\n".encode())
    