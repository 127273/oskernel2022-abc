#ifndef __SYSCALL_H
#define __SYSCALL_H

// System call numbers
#define SYS_fork        1001
#define SYS_exit        1002
#define SYS_wait        1003
#define SYS_pipe        1004
#define SYS_read        1005
#define SYS_kill        1006
#define SYS_exec        1007
#define SYS_fstat       1008
#define SYS_chdir       1009
#define SYS_dup         1010
#define SYS_getpid      1011
#define SYS_sbrk        1012
#define SYS_sleep       1013
#define SYS_uptime      1014
#define SYS_open        1015
#define SYS_write       1016
#define SYS_remove      1017
#define SYS_trace       1018
#define SYS_sysinfo     1019
#define SYS_mkdir       1020
#define SYS_close       1021
#define SYS_test_proc   1022
#define SYS_dev         1023
#define SYS_dir         1024
#define SYS_getcwd      1025
//prebuilt syscalls

//new syscalls
#define SYS_K_getcwd        2001
#define SYS_K_clone         2002
#define SYS_K_getppid       2003
#define SYS_K_openat        2004
#define SYS_K_dup3          2005
#define SYS_K_wait          2006
#define SYS_K_mkdirat       2007
#define SYS_K_yield         2008
#define SYS_K_times         2009
#define SYS_K_brk           2010
#define SYS_K_uname         2011
#define SYS_K_fstat         2012
#define SYS_K_getdents      2013
#define SYS_K_gettimeofday  2014
#define SYS_K_mount         2015
#define SYS_K_umount        2016
#define SYS_K_mmap          2017
#define SYS_K_munmap        2018
#define SYS_K_unlink        2019
#define SYS_K_sleep         2020
#define SYS_K_getuid        2021
#define SYS_K_geteuid       2022
#define SYS_K_getgid        2023
#define SYS_K_getegid       2024
#define SYS_K_readlinkat    2025
#define SYS_K_ioctl         2026
#define SYS_K_rt_sigaction  2027
#define SYS_K_getpgid       2028
#define SYS_K_rt_sigprocmask 2029
#define SYS_K_ppoll         2030
#define SYS_K_fstatat       2031
#define SYS_K_fcntl         2032
#define SYS_K_sendfile      2033
#define SYS_K_exitgroup     2034

#endif