
#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "riscv.h"
#include "spinlock.h"
#include "proc.h"
#include "syscall.h"
#include "defs.h"
#include "sysinfo.h"

inline void InterruptMapping(int *num);

// Fetch the uint64 at addr from the current process.
int fetchaddr(uint64 addr, uint64 *ip)
{
  struct proc *p = myproc();
  if (addr >= p->heap_addr || addr + sizeof(uint64) > p->heap_addr)
    return -1;
  if (copyin(p->pagetable, (char *)ip, addr, sizeof(*ip)) != 0)
    return -1;
  return 0;
}

// Fetch the nul-terminated string at addr from the current process.
// Returns length of string, not including nul, or -1 for error.
int fetchstr(uint64 addr, char *buf, int max)
{
  struct proc *p = myproc();
  int err = copyinstr(p->pagetable, buf, addr, max);
  if (err < 0)
    return err;
  return strlen(buf);
}

static uint64
argraw(int n)
{
  struct proc *p = myproc();
  switch (n)
  {
  case 0:
    return p->trapframe->a0;
  case 1:
    return p->trapframe->a1;
  case 2:
    return p->trapframe->a2;
  case 3:
    return p->trapframe->a3;
  case 4:
    return p->trapframe->a4;
  case 5:
    return p->trapframe->a5;
  }
  panic("argraw");
  return -1;
}

// Fetch the nth 32-bit system call argument.
int argint(int n, int *ip)
{
  *ip = argraw(n);
  return 0;
}

// Retrieve an argument as a pointer.
// Doesn't check for legality, since
// copyin/copyout will do that.
int argaddr(int n, uint64 *ip)
{
  *ip = argraw(n);
  return 0;
}

// Fetch the nth word-sized system call argument as a null-terminated string.
// Copies into buf, at most max.
// Returns string length if OK (including nul), -1 if error.
int argstr(int n, char *buf, int max)
{
  uint64 addr;
  if (argaddr(n, &addr) < 0)
    return -1;
  return fetchstr(addr, buf, max);
}

extern uint64 sys_chdir(void);
extern uint64 sys_close(void);
extern uint64 sys_dup(void);
extern uint64 sys_exec(void);
extern uint64 sys_exit(void);
extern uint64 sys_fork(void);
extern uint64 sys_fstat(void);
extern uint64 sys_getpid(void);
extern uint64 sys_kill(void);
extern uint64 sys_mkdir(void);
extern uint64 sys_open(void);
extern uint64 sys_pipe(void);
extern uint64 sys_read(void);
extern uint64 sys_sbrk(void);
extern uint64 sys_sleep(void);
extern uint64 sys_wait(void);
extern uint64 sys_write(void);
extern uint64 sys_uptime(void);
extern uint64 sys_test_proc(void);
extern uint64 sys_dev(void);
extern uint64 sys_dir(void);
extern uint64 sys_getcwd(void);
extern uint64 sys_remove(void);
extern uint64 sys_trace(void);
extern uint64 sys_sysinfo(void);

/*------ add by NH ------*/
extern uint64 sys_K_getcwd(void);
extern uint64 sys_K_clone(void);
extern uint64 sys_K_getppid(void);
extern uint64 sys_K_openat(void);
extern uint64 sys_K_dup3(void);
extern uint64 sys_K_wait(void);
extern uint64 sys_K_mkdirat(void);
extern uint64 sys_K_yield(void);
extern uint64 sys_K_times(void);
extern uint64 sys_K_brk(void);
extern uint64 sys_K_uname(void);
extern uint64 sys_K_fstat(void);
extern uint64 sys_K_getdents(void);
extern uint64 sys_K_gettimeofday(void);
extern uint64 sys_K_mount(void);
extern uint64 sys_K_umount(void);
extern uint64 sys_K_mmap(void);
extern uint64 sys_K_munmap(void);
extern uint64 sys_K_unlink(void);
extern uint64 sys_K_sleep(void);
extern uint64 sys_K_getuid(void);
extern uint64 sys_K_geteuid(void);
extern uint64 sys_K_getgid(void);
extern uint64 sys_K_getegid(void);
extern uint64 sys_K_readlinkat(void);
extern uint64 sys_K_ioctl(void);
extern uint64 sys_K_rt_sigprocmask(void);
extern uint64 sys_K_rt_sigaction(void);
extern uint64 sys_K_getpgid(void);
extern uint64 sys_K_ppoll(void);
extern uint64 sys_K_fstatat(void);
extern uint64 sys_K_sendfile(void);
extern uint64 sys_K_fcntl(void);
extern uint64 sys_K_exitgroup(void);

/*---------------------------------*/

static uint64 (*syscalls[])(void) = {
    [SYS_fork] sys_fork,
    [SYS_exit] sys_exit,
    [SYS_wait] sys_wait,
    [SYS_pipe] sys_pipe,
    [SYS_read] sys_read,
    [SYS_kill] sys_kill,
    [SYS_exec] sys_exec,
    [SYS_fstat] sys_fstat,
    [SYS_chdir] sys_chdir,
    [SYS_dup] sys_dup,
    [SYS_getpid] sys_getpid,
    [SYS_sbrk] sys_sbrk,
    [SYS_sleep] sys_sleep,
    [SYS_uptime] sys_uptime,
    [SYS_open] sys_open,
    [SYS_write] sys_write,
    [SYS_mkdir] sys_mkdir,
    [SYS_close] sys_close,
    [SYS_test_proc] sys_test_proc,
    [SYS_dev] sys_dev,
    [SYS_dir] sys_dir,
    [SYS_getcwd] sys_getcwd,
    [SYS_remove] sys_remove,
    [SYS_trace] sys_trace,
    [SYS_sysinfo] sys_sysinfo,

    /*-------- add by NH ---------*/
    [SYS_K_getcwd] sys_K_getcwd,
    [SYS_K_clone] sys_K_clone,
    [SYS_K_getppid] sys_K_getppid,
    [SYS_K_openat] sys_K_openat,
    [SYS_K_dup3] sys_K_dup3,
    [SYS_K_wait] sys_K_wait,
    [SYS_K_mkdirat] sys_K_mkdirat,
    [SYS_K_yield] sys_K_yield,
    [SYS_K_times] sys_K_times,
    [SYS_K_brk] sys_K_brk,
    [SYS_K_uname] sys_K_uname,
    [SYS_K_fstat] sys_K_fstat,
    [SYS_K_getdents] sys_K_getdents,
    [SYS_K_gettimeofday] sys_K_gettimeofday,
    [SYS_K_mount] sys_K_mount,
    [SYS_K_umount] sys_K_umount,
    [SYS_K_mmap] sys_K_mmap,
    [SYS_K_munmap] sys_K_munmap,
    [SYS_K_unlink] sys_K_unlink,
    [SYS_K_sleep] sys_K_sleep,
    [SYS_K_getuid] sys_K_getuid,
    [SYS_K_geteuid] sys_K_geteuid,
    [SYS_K_getgid] sys_K_getgid,
    [SYS_K_getegid] sys_K_getegid,
    [SYS_K_readlinkat] sys_K_readlinkat,
    [SYS_K_ioctl] sys_K_ioctl,
    [SYS_K_rt_sigaction] sys_K_rt_sigaction,
    [SYS_K_rt_sigprocmask] sys_K_rt_sigprocmask,
    [SYS_K_getpgid] sys_K_getpgid,
    [SYS_K_ppoll] sys_K_ppoll,
    [SYS_K_fstatat] sys_K_fstatat,
    [SYS_K_sendfile] sys_K_sendfile,
    [SYS_K_fcntl] sys_K_fcntl,
    [SYS_K_exitgroup] sys_K_exitgroup,
};  

void syscall(void)
{
  static char *sysnames[] = {
      [SYS_fork] "fork",
      [SYS_exit] "exit",
      [SYS_wait] "wait",
      [SYS_pipe] "pipe",
      [SYS_read] "read",
      [SYS_kill] "kill",
      [SYS_exec] "exec",
      [SYS_fstat] "fstat",
      [SYS_chdir] "chdir",
      [SYS_dup] "dup",
      [SYS_getpid] "getpid",
      [SYS_sbrk] "sbrk",
      [SYS_sleep] "sleep",
      [SYS_uptime] "uptime",
      [SYS_open] "open",
      [SYS_write] "write",
      [SYS_mkdir] "mkdir",
      [SYS_close] "close",
      [SYS_test_proc] "test_proc",
      [SYS_dev] "dev",
      [SYS_dir] "dir",
      [SYS_getcwd] "getcwd",
      [SYS_remove] "remove",
      [SYS_trace] "trace",
      [SYS_sysinfo] "sysinfo",

      /*-------- add by NH ---------*/
      [SYS_K_getcwd] "K_getcwd",
      [SYS_K_clone] "K_clone",
      [SYS_K_getppid] "K_getppid",
      [SYS_K_openat] "K_openat",
      [SYS_K_dup3] "K_dup3",
      [SYS_K_wait] "K_wait",
      [SYS_K_mkdirat] "K_mkdirat",
      [SYS_K_yield] "K_yield",
      [SYS_K_times] "K_times",
      [SYS_K_brk] "K_brk",
      [SYS_K_uname] "K_uname",
      [SYS_K_fstat] "K_fstat",
      [SYS_K_getdents] "K_getdents",
      [SYS_K_gettimeofday] "K_gettimeofday",
      [SYS_K_mount] "K_mount",
      [SYS_K_umount] "K_umount",
      [SYS_K_mmap] "K_mmap",
      [SYS_K_munmap] "K_munmap",
      [SYS_K_unlink] "K_unlink",
      [SYS_K_sleep] "K_sleep",
      [SYS_K_getuid] "K_getuid",
      [SYS_K_geteuid] "K_geteuid",
      [SYS_K_getgid] "K_getgid",
      [SYS_K_getegid] "K_getegid",
      [SYS_K_readlinkat] "K_readlinkat",
      [SYS_K_ioctl] "K_ioctl",
      [SYS_K_rt_sigaction] "K_rt_sigaction",
      [SYS_K_rt_sigprocmask] "K_rt_sigprocmask",
      [SYS_K_getpgid] "K_getpgid",
      [SYS_K_ppoll] "K_ppoll",
      [SYS_K_fstatat] "K_fstatat",
      [SYS_K_sendfile] "K_sendfile",
      [SYS_K_fcntl] "K_fcntl",
      [SYS_K_exitgroup] "K_exitgroup",

  };
  int num;
  struct proc *p = myproc();

  num = p->trapframe->a7;
  InterruptMapping(&num);

  if (num > 0 && num < NELEM(syscalls) && syscalls[num])
  {
    
    uint64 ret = 0;
    if (num != SYS_write && num != SYS_dir && num != SYS_read)
    {
      ret = syscalls[num]();
      p->trapframe->a0 = ret;
      #ifdef READSYSCALL
      struct trapframe *tf = p->trapframe;
      printf("syscall -- %s(0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x), return 0x%x\n", 
        sysnames[num], tf->a0, tf->a1, tf->a2, tf->a3, tf->a4, tf->a5, ret);
      #endif
    }
    else
    {
      p->trapframe->a0 = syscalls[num]();
    }
    // trace
    if ((p->tmask & (1 << num)) != 0)
    {
      printf("pid %d: syscall %s -> %d\n", p->pid, sysnames[num], p->trapframe->a0);
    }
  }
  else
  {
    printf("pid %d %s: unknown sys call %d\n",
           p->pid, p->name, num);
    p->trapframe->a0 = 0;
  }
}

uint64
sys_test_proc(void)
{
  int n;
  argint(0, &n);
  printf("hello world from proc %d, hart %d, arg %d\n", myproc()->pid, r_tp(), n);
  return 0;
}

uint64
sys_sysinfo(void)
{
  uint64 addr;
  struct proc *p = myproc();

  if (argaddr(0, &addr) < 0)
  {
    return -1;
  }

  struct sysinfo info;
  info.freemem = freemem_amount();
  info.nproc = procnum();

  if (copyout(p->pagetable, addr, (char *)&info, sizeof(info)) < 0)
  {
    return -1;
  }

  return 0;
}

/*-------------------------- add by NH below -----------------------------------*/
inline void InterruptMapping(int *num)
{
  switch (*num)
  {
  case 64:
    *num = SYS_write; //write, success
    break;
  case 93:
    *num = SYS_exit; //exit, success
    break;
  case 49:
    *num = SYS_chdir; //chdir, success
    break;
  case 57:
    *num = SYS_close; //close, success
    break;
  case 17:
    *num = SYS_K_getcwd; //getcwd, success
    break;
  case 172:
    *num = SYS_getpid; //getpid, success
    break;
  case 260:
    *num = SYS_K_wait; //wait, waitpid, success
    break;
  case 34:
    *num = SYS_K_mkdirat; //mkdirat, success
    break;
  case 63:
    *num = SYS_read; //read, success
    break;
  case 56:
    *num = SYS_K_openat; //openat, success
    break;
  case 23:
    *num = SYS_dup; //dup, success
    break;
  case 59:
    *num = SYS_pipe; //pipe, question
    break;
  case 220:
    *num = SYS_K_clone; //clone, success
    break;
  case 221:
    *num = SYS_exec; //execve, success
    break;
  case 173:
    *num = SYS_K_getppid; //getppid, success
    break;
  case 24:
    *num = SYS_K_dup3; //dup3, success
    break;
  case 80:
    *num = SYS_K_fstat; //fstat, success
    break;
  case 101:
    *num = SYS_K_sleep; //nanosleep, question
    break;
  case 124:
    *num = SYS_K_yield; //yield, question
    break;
  case 153:
    *num = SYS_K_times; //times, question
    break;
  case 214:
    *num = SYS_K_brk; //brk
    break;
  case 160:
    *num = SYS_K_uname; //uname, success
    break;
  case 61:
    *num = SYS_K_getdents; //getdents
    break;
  case 169:
    *num = SYS_K_gettimeofday; //gettimeofady
    break;
  case 39:
    *num = SYS_K_umount; // umount2
    break;
  case 40:
    *num = SYS_K_mount; // mount
    break;
  case 222:
    *num = SYS_K_mmap; // mmap
    break;
  case 215:
    *num = SYS_K_munmap; // munmap
    break;
  case 35:
    *num = SYS_K_unlink; // unlinkat
    break;
  case 174:
    *num = SYS_K_getuid; // getuid
    break;
  case 175:
    *num = SYS_K_geteuid; // geteuid
    break;
  case 176:
    *num = SYS_K_getgid; // getgid
    break;
  case 177:
    *num = SYS_K_getegid; // getegid
    break;
  case 78:
    *num = SYS_K_readlinkat; // readlinkat
    break;
  case 29:
    *num = SYS_K_ioctl; // ioctl
    break;
  case 135:
    *num = SYS_K_rt_sigprocmask; // rt_sigprocmask
    break;
  case 134:
    *num = SYS_K_rt_sigaction; // rt_sigaction
    break;
  case 155:
    *num = SYS_K_getpgid; // getpgid
    break;
  case 73:
    *num = SYS_K_ppoll; // ppoll
    break;
  case 79:
    *num = SYS_K_fstatat; // fstatat
    break;
  case 25:
    *num = SYS_K_fcntl; // fcntl
    break;
  case 71:
    *num = SYS_K_sendfile; // sendfile
    break;
  case 94:
    *num = SYS_K_exitgroup; // exit_group
    break;
  default:
    break;
  }
}
